# fuzzy-model.coffee

fs = require 'fs'
path = require 'path'
assert = require 'assert'

CSON = require 'cson'
yargs = require 'yargs'
_ = require 'lodash'
debug = require('debug')('learning:fuzzy-model')

[eq, geq, leq] = require '../fuzzy-learning/lib/equalish'

FuzzyController = require '../fuzzy-controller/src/fuzzy-controller'
Dataset = require '../fuzzy-learning/lib/dataset'
SetBuilder = require '../fuzzy-learning/lib/set-builder'
RuleBuilder = require '../fuzzy-learning/lib/rule-builder'

js = JSON.stringify

argv = yargs
  .usage('Usage: $0 <command> [options] <csv>')
  .demand(2)
  .command('create <csv>', 'Create a new model from the data file',
    "rule-builder":
      alias: "r"
      description: "Rule builder to use"
      choices: ["genetic", "lfe", "mlfe", "flfe", "wuchen"]
      default: "lfe"
    "set-builder":
      alias: "s"
      description: "Set builder to use"
      choices: ["cluster", "range"]
      default: "cluster"
  )
  .command('train <model> <csv>', 'Train the model from the data file'
    trainer:
      alias: "t"
      description: "Training algorithm to use"
      choices: ["gradient-descent", "newtons-method"]
      default: "gradient-descent"
  )
  .command('test <model> <csv>', 'Test the model against the data file')
  .demand('o')
  .alias('o', 'output')
  .describe('o', "Comma-delimited output column names")
  .alias('g', 'ignore')
  .describe('g', "Columns to ignore")
  .alias('d', 'codes')
  .describe('d', "Inputs that should have mutually exclusive sets")
  .help('h')
  .alias('h', 'help')
  .argv

parseModel = (modelFile) ->

  ext = path.extname(modelFile).toLowerCase()

  switch ext
    when '.cson'
      model = CSON.parseCSONFile modelFile
    when '.json'
      str = fs.readFileSync(modelFile, 'utf-8')
      model = JSON.parse(str)
    else
      throw new Error "Unrecognized file extension for model file #{modelFile}"

  model

parseCSV = (csvFile, ignore) ->

  contents = fs.readFileSync(csvFile, "utf8")

  lines = _.filter contents.split("\n"), (line) -> line.length > 0

  headers = lines.shift().split(",")

  ignoreindex = null
  if ignore?
    ignoreindex = _.indexOf headers, ignore
    if ignoreindex != -1
      headers.splice ignoreindex, 1

  varNames = _.map headers, _.camelCase

  rows = lines.map (line) ->
    row = line.split(",").map (item, i) ->
      if item.length == 0
        undefined
      else if item.match(/^-?(?:0|[1-9]\d*)(?:\.\d+)?(?:[eE][+-]?\d+)?$/)
        parseFloat item, 10
      else item
    if ignoreindex? and ignoreindex != -1
      row.splice(ignoreindex, 1)
      assert (row.length <= varNames.length),
        "Row length == #{row.length}, varNames length == #{varNames.length}"
    row

  contents = null
  lines = null
  headers = null

  [rows, varNames]

createModel = (alg, rows, varNames, outputVarNames, codes) ->

  model = alg rows, varNames, outputVarNames, codes

  console.log CSON.stringify _.omit model, ["parsed_rules"]

trainModel = (model, rows, varNames, outputVarNames) ->

  fc = new FuzzyController model
  tr = new GradientDescentTrainer fc, [], 1, 0

  i = 0

  for row in rows
    data = _.zipObject varNames, row
    inputData = _.omit data, outputVarNames
    inputData = _(inputData).omitBy(_.isUndefined).omitBy(_.isNull).value()
    outputData = _.pick data, outputVarNames
    tr.train({inputs: inputData, outputs: outputData})

  json = fc.toJSON()

  console.log CSON.stringify(_.omit(json, 'parsed_rules'))

ape = (actual, expected) ->
  if eq(expected, 0)
    if eq(actual, 0)
      0
    else
      undefined
  else
    Math.abs(actual - expected) / Math.abs(expected)

testModel = (model, rows, varNames, outputVarNames) ->

  predVarNames = _.map outputVarNames, (name) -> _.camelCase "actual #{name}"
  errorVarNames = _.map outputVarNames, (name) -> _.camelCase "error #{name}"

  fc = new FuzzyController model

  vn = varNames.join(',')
  pvn = predVarNames.join(',')
  evn = errorVarNames.join(',')

  console.log "#{vn},#{pvn},#{evn}"

  for row in rows
    data = _.zipObject varNames, row
    inputData = _.omit data, outputVarNames
    inputData = _(inputData).omitBy(_.isUndefined).omitBy(_.isNull).value()
    outputData = _.pick data, outputVarNames
    pred = fc.evaluate inputData
    debug "#{js inputData} => #{js pred}"
    errors = _.mapValues pred, (value, key) ->
      ape value, outputData[key]

    rs = row.join(",")
    ps = _.map(outputVarNames, (name) -> pred[name]).join(",")
    es = _.map(outputVarNames, (name) -> errors[name]).join(",")

    console.log "#{rs},#{ps},#{es}"

makeModel = (sets, rules) ->
  model =
    inputs: sets.inputs
    outputs: sets.outputs
    rules: rules
  model

displayModel = (model) ->
  console.log JSON.stringify(model, null, "  ")

main = (argv) ->

  outputNames = if argv.o then argv.o.split /,\s*/ else []
  ignore = if argv.g then argv.g else null
  codes = if argv.d then argv.d.split /,\s*/ else []

  [rows, varNames] = parseCSV argv.csv

  if outputNames.length == 0
    outputNames = [varNames[varNames.length - 1]]

  dataset = new Dataset rows, varNames, outputNames

  command = argv._[0]

  switch command
    when "create"
      setBuilder = SetBuilder.load argv.s
      sets = setBuilder.buildSets dataset
      # For columns that are discrete codes, make special sets and overwrite
      if codes? and codes.length > 0
        partitionBuilder = SetBuilder.load "partition"
        partitions = {}
        for code in codes
          column = dataset.getColumn code
          partitions[code] = partitionBuilder.buildSetsForColumn column
        _.assign sets, partitions
      ruleBuilder = RuleBuilder.load argv.r
      parsed_rules = ruleBuilder.buildRules dataset, sets
      model = makeModel sets, parsed_rules
      displayModel model
    when "train"
      model = parseModel argv.model
      trainer = Trainer.load argv.t
      trained = trainer.train model, dataset
      displayModel trained
    when "test"
      model = parseModel argv.model
      testDataset = testModel model, dataset
      displayDataset testDataset
    else
      throw new Error("Unrecognized command: #{command}")

try
  main argv
  process.exit 0
catch error
  console.error error
  process.exit 1
