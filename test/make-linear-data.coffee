# plane.coffee
# Create a data file with a plane (x, y, z) according to
#
# z = ax + by + c

argv = require('yargs')
  .default('m', 0)
  .default('b', 0)
  .default('n', 100)
  .argv

{m, b, n} = argv

console.log "x,y"
for i in [0..n]
  x = Math.random() * 5.0
  y = m * x + b
  console.log "#{x},#{y}"
