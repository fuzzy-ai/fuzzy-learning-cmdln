# kim-function.coffee

_ = require 'lodash'

f = (x1, x2) ->
  Math.pow(1 + Math.pow(x1, -2) + Math.pow(x2, -1.5), 2)

count = if process.argv.length >= 3 then parseInt(process.argv[2]) else 50

console.log "x1,x2,y"
_.times count, () ->
  x1 = _.random 1.0, 5.0, true
  x2 = _.random 1.0, 5.0, true
  y = f x1, x2
  console.log "#{x1},#{x2},#{y}"
