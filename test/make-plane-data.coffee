# plane.coffee
# Create a data file with a plane (x, y, z) according to
#
# z = ax + by + c

argv = require('yargs')
  .default('a', 0)
  .default('b', 0)
  .default('c', 1)
  .default('n', 100)
  .argv

{a, b, c, n} = argv

console.log "x,y,z"
for i in [0..n]
  x = Math.random() * 5.0
  y = Math.random() * 5.0
  z = a * x + b * y + c
  console.log "#{x},#{y},#{z}"
