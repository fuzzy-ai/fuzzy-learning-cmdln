# quadratic.coffee
# Test using learning a linear relation

argv = require('yargs')
  .usage('Usage: $0 -a<quadratic> -b<linear> -c<constant>')
  .help('h')
  .alias('h', 'help')
  .default('a', 1)
  .alias('a', 'quadratic')
  .number('a')
  .describe('a', 'quadratic coefficient; the x^2 part')
  .default('b', 0)
  .alias('b', 'linear')
  .number('b')
  .describe('b', 'linear coefficient; the x part')
  .default('c', 0)
  .alias('c', 'constant')
  .number('c')
  .describe('c', 'constant; the y intercept')
  .argv

trainToFunction = require './train-to-function'

ITERATIONS = 1
SAMPLES = 3

BOUNDARY = 100

initial =
  inputs:
    x:
      veryLow: [0, BOUNDARY/4]
      low: [0, BOUNDARY/4, BOUNDARY/2]
      medium: [BOUNDARY/4, BOUNDARY/2, 3*BOUNDARY/4]
      high: [BOUNDARY/2, 3*BOUNDARY/4, BOUNDARY]
      veryHigh: [3*BOUNDARY/4, BOUNDARY]
  outputs:
    y:
      veryLow: [0, BOUNDARY/4]
      low: [0, BOUNDARY/4, BOUNDARY/2]
      medium: [BOUNDARY/4, BOUNDARY/2, 3*BOUNDARY/4]
      high: [BOUNDARY/2, 3*BOUNDARY/4, BOUNDARY]
      veryHigh: [3*BOUNDARY/4, BOUNDARY]
  rules: [
    "if x is veryLow then y is veryLow WITH 0.5"
    "if x is low then y is low WITH 0.5"
    "if x is medium then y is medium WITH 0.5"
    "if x is high then y is high WITH 0.5"
    "if x is veryHigh then y is veryHigh WITH 0.5"
  ]

main = (argv) ->

  {a, b, c} = argv

  func = (x) -> a * (x * x) + b * x + c

  trainToFunction initial, func, 0, BOUNDARY, ITERATIONS, SAMPLES

main(argv)
