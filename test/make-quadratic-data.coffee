# quadratic.coffee
# Test using learning a linear relation

argv = require('yargs')
  .usage('Usage: $0 -a<quadratic> -b<linear> -c<constant>')
  .help('h')
  .alias('h', 'help')
  .default('a', 1)
  .alias('a', 'quadratic')
  .number('a')
  .describe('a', 'quadratic coefficient; the x^2 part')
  .default('b', 0)
  .alias('b', 'linear')
  .number('b')
  .describe('b', 'linear coefficient; the x part')
  .default('c', 0)
  .alias('c', 'constant')
  .number('c')
  .describe('c', 'constant; the y intercept')
  .number('n')
  .describe('n', 'number of data points to generate')
  .default('n', 100)
  .argv

main = (argv) ->

  {a, b, c, n} = argv

  func = (x) -> a * (x * x) + b * x + c

  console.log "x, y"
  
  for i in [0..n]
    x = Math.random() * 100.0
    y = func x
    console.log "#{x},#{y}"

main(argv)
