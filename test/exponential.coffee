# quadratic.coffee
# Test using learning a linear relation

argv = require('yargs')
  .default('a', 1)
  .default('b', 0.046052)
  .default('c', 0)
  .argv

trainToFunction = require './train-to-function'

ITERATIONS = 1
SAMPLES = 0

BOUNDARY = 100

initial =
  inputs:
    x:
      veryLow: [0, BOUNDARY/4]
      low: [0, BOUNDARY/4, BOUNDARY/2]
      medium: [BOUNDARY/4, BOUNDARY/2, 3*BOUNDARY/4]
      high: [BOUNDARY/2, 3*BOUNDARY/4, BOUNDARY]
      veryHigh: [3*BOUNDARY/4, BOUNDARY]
  outputs:
    y:
      veryLow: [0, BOUNDARY/4]
      low: [0, BOUNDARY/4, BOUNDARY/2]
      medium: [BOUNDARY/4, BOUNDARY/2, 3*BOUNDARY/4]
      high: [BOUNDARY/2, 3*BOUNDARY/4, BOUNDARY]
      veryHigh: [3*BOUNDARY/4, BOUNDARY]
  rules: [
    "x INCREASES y WITH 0.5"
  ]

main = (argv) ->

  {a, b, c} = argv

  func = (x) -> a * Math.exp(x * b) + c

  trainToFunction initial, func, 0, BOUNDARY, ITERATIONS, SAMPLES

main(argv)
